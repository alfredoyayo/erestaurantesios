//
//  ERUIShadowLabel.swift
//  ERestaurantes
//
//  Created by Mac on 2/23/21.
//

import UIKit

@IBDesignable class ERUIShadowLabel: ERUIShapeLabel, ERShadow{
    
    //MARK: - SHADOW
    
    var shadowStyle: ERShadowStyle = ERShadowStyle() {
        didSet { self.updateShadowAppereance() }
    }
    
    @IBInspectable internal var shadowRadius: CGFloat {
        get { self.shadowStyle.radius }
        set { self.shadowStyle.radius = newValue }
    }
    
    @IBInspectable internal var shadowOpacity: Float {
        get { self.shadowStyle.opacity }
        set { self.shadowStyle.opacity = newValue }
    } 
}
