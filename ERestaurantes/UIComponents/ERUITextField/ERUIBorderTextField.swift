//
//  ERUIBorderTextField.swift
//  ERestaurantes
//
//  Created by Mac on 2/23/21.
//

import UIKit

@IBDesignable class ERUIBorderTextField: UITextField, ERBorder {
    
    //MARK: - BORDER
    var borderStyleER: ERBorderStyle = ERBorderStyle() {
        didSet { self.updateBorderAppereance() }
    }
    
    @IBInspectable internal var borderColor: UIColor {
        get { self.borderStyleER.color }
        set { self.borderStyleER.color = newValue }
    }
    
    @IBInspectable internal  var borderWidth: CGFloat {
        get { self.borderStyleER.width }
        set { self.borderStyleER.width = newValue }
    }
}
