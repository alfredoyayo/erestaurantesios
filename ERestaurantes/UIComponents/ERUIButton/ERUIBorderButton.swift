//
//  ERUIBorderButton.swift
//  ERestaurantes
//
//  Created by Kenyi Rodriguez on 20/02/21.
//

import UIKit

@IBDesignable class ERUIBorderButton: UIButton, ERBorder {
    
    //MARK: - BORDER
    var borderStyleER: ERBorderStyle = ERBorderStyle() {
        didSet { self.updateBorderAppereance() }
    }
    
    @IBInspectable internal var borderColor: UIColor {
        get { self.borderStyleER.color }
        set { self.borderStyleER.color = newValue }
    }
    
    @IBInspectable internal  var borderWidth: CGFloat {
        get { self.borderStyleER.width }
        set { self.borderStyleER.width = newValue }
    }
}
