//
//  ERUIShadowView.swift
//  ERestaurantes
//
//  Created by Mac on 2/23/21.
//

import UIKit

@IBDesignable class ERUIShadowView: ERUIShapeView, ERShadow{
    
    //MARK: - SHADOW
    
    var shadowStyle: ERShadowStyle = ERShadowStyle() {
        didSet { self.updateShadowAppereance() }
    }
    
    @IBInspectable internal var shadowColor: UIColor {
        get { self.shadowStyle.color }
        set { self.shadowStyle.color = newValue }
    }
    
    @IBInspectable internal var shadowRadius: CGFloat {
        get { self.shadowStyle.radius }
        set { self.shadowStyle.radius = newValue }
    }
    
    @IBInspectable internal var shadowOpacity: Float {
        get { self.shadowStyle.opacity }
        set { self.shadowStyle.opacity = newValue }
    }
    
    @IBInspectable internal var shadowOffset: CGSize {
        get { self.shadowStyle.offset }
        set { self.shadowStyle.offset = newValue }
    } 
}
